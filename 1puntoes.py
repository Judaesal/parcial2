from pyfirmata import Arduino, util
from tkinter import *
from tkinter import messagebox
from PIL import Image , ImageTk
from time import sleep

cuenta=0
counter =0
counter1 = 0
counter2 = 0

ventana = Tk()
ventana.state('zoomed') #iniciar la pantalla maximizada
ventana.geometry('1200x800')
ventana.configure(bg = 'white')
ventana.title("Cronometro")
draw = Canvas(ventana, width=1900, height=980)
draw.place(x = 80,y = 80)

def update_label():
    global cuenta, counter, counter1,counter2
    counter += 1
    dato2['text'] = str(counter)
    if (counter==60):
        counter=0
        counter1 += 1
        dato1['text'] = str(counter1)
        if (counter1==60):
            counter1=0
            counter2 += 1
            dato['text'] = str(counter2)
    cuenta=your_label.after(500, update_label)

def pause():
    global cuenta
    dato.after_cancel(cuenta)
    

your_label=Label(ventana)
your_label.pack()
start_button=Button(ventana,text="start",command=update_label)
start_button.place(x=120, y=140)
pause=Button(ventana,text="pause",command=pause)
pause.place(x=120, y=170)
reiniciar=Button(ventana,text="reiniciar",command=(cuenta==0,counter==0, counter1==0,counter2==0))
reiniciar.place(x=120, y=230)
stop=Button(ventana,text="parar",command=ventana.destroy)
stop.place(x=120, y=200)
dato=Label(ventana, text="00", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
dato.place(x=40, y=90)
dato1=Label(ventana, text="00", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
dato1.place(x=80, y=90)
dato2=Label(ventana, text="00", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
dato2.place(x=120, y=90)

ventana.mainloop()
